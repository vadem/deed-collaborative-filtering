﻿using System.Collections.Generic;
using System.Linq;
using Filter.Models;

namespace Filter
{
    class Filter
    {
        static readonly List<Course> Courses = new List<Course>
        {
            new Course { Id = 1, Name = "Rocket Science" },
            new Course { Id = 2, Name = "Mathematics" },
            new Course { Id = 3, Name = "English Language" },
            new Course { Id = 4, Name = "Chemistry" },
            new Course { Id = 5, Name = "Physics" },
            new Course { Id = 6, Name = "Computer Science" },
        }; 
        
        /// <summary>
        /// Получаем пример пользователя с выбранными предпочтениями по курсам.
        /// </summary>
        static User GetExampleUser()
        {
            return new User
            {
                Id = 1,
                FirstName = "John",
                LastName = "Preston",
                PreferredCourses = Courses.Take(3).ToList()
            };
        }

        /// <summary>
        /// Получаем список пользователей с выбранными курсами для сравнения.
        /// </summary>
        static List<User> GetExampleUsersToCompare()
        {
            return new List<User>
            {
                new User { Id = 2, PreferredCourses = Courses.Skip(2).Take(3).ToList() },
                new User { Id = 4, PreferredCourses = Courses.Skip(1).Take(3).ToList() },
                new User { Id = 3, PreferredCourses = Courses.Skip(3).Take(3).ToList() },
            };
        }
        
        static void Main(string[] args)
        {
            var user = GetExampleUser();
            var otherUsers = GetExampleUsersToCompare();

            // Получаем список пользователей, предпочтения которых полностью аналогичны пользователю.
            var identicalUsers = otherUsers
                .Where(x => x.PreferredCourses.All(y => user.PreferredCourses.Contains(y)))
                .ToList();
            
            // Получаем список пользователей, которые выбрали 2 из 3 курсов, выбранных пользователем.
            var similarUsers = otherUsers
                .Where(x => x.PreferredCourses.Intersect(user.PreferredCourses).Count() == 2)
                .ToList();
            
            // Получаем список пользователей, которые выбрали хотя бы 1 из 3 курсов, выбранных пользователем.
            var notSoSimilarUsers = otherUsers
                    .Where(x => x.PreferredCourses.Any(y => user.PreferredCourses.Contains(y)))
                    .ToList();
            
            // И наконец, список пользователей, которые не выбрали ни одного курса, который есть у пользователя.
            var absolutelyDifferentUsers = otherUsers
                .Where(x => !x.PreferredCourses.Any(y => user.PreferredCourses.Contains(y)))
                .ToList();
        }
    }
}