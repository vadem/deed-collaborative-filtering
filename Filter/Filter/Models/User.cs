using System.Collections.Generic;

namespace Filter.Models
{
    public class User
    {
        public long Id { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        /// <summary>
        /// Предпочитаемые курсы пользователя (3 курса на выбор).
        /// </summary>
        public List<Course> PreferredCourses { get; set; } 
    }
}